+++
title = "xmpp.rs"


# The homepage contents
[extra]
title_img = "/logo.png"
title_img_lines = "7"
lead = "Real-time federated communications for your favorite programming language"
url = "https://gitlab.com/xmpp-rs/xmpp-rs"
url_button = "Checkout our repository"
repo_version = "v0.5.0"
repo_license = "MPL 2.0"
repo_url = "https://crates.io/crates/xmpp"

[[extra.list]]
title = "Federation"
content = "Develop the next messaging application without locking users in a closed ecosystem where their data is trapped"

[[extra.list]]
title = "Interoperability"
content = "XMPP protocol is developed in the open with the <a href='https://xmpp.org'>XSF</a>, and has many implementations for clients and servers"

[[extra.list]]
title = "Durability"
content = "For almost 20 years now, XMPP has been an Internet standard (<a href='https://datatracker.ietf.org/doc/rfc6120/'>RFC</a>). Like IRC or HTTP, it's not going anywhere"

[[extra.list]]
title = "Correctness"
content = "The rust compiler does the heavy lifting ; <i>null pointer exceptions</i> are a thing of the past"

[[extra.list]]
title = "Developer UX"
content = "If you find something hard, it's a bug and should be reported/fixed"

+++
