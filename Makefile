WEBROOT ?= "/var/www/xmpp.rs"

all: public

deploy: update clean drafts public
	mv drafts public/_drafts
	rm -rf $(WEBROOT)
	mv public $(WEBROOT)

update:
	git pull -r
	git submodule init && git submodule update

public: clean
	zola build

drafts:
	zola build --drafts --base-url "https://xmpp.rs/_drafts" --output-dir drafts

serve:
	zola serve --drafts

clean:
	rm -rf public drafts
